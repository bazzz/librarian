package libraria

import (
	"encoding/xml"
	"strconv"

	"gitlab.com/bazzz/dates"
	"gitlab.com/bazzz/file"
	"gitlab.com/bazzz/tmdb"
)

func newEpisode(input *tmdb.Episode) Episode {
	e := Episode{
		Title:     input.Title,
		Season:    input.Season,
		Number:    input.Number,
		Premiered: &input.Premiered,
		Plot:      input.Overview,
		Image:     input.Image,
	}
	return e
}

// Episode represents a movie.
type Episode struct {
	XMLName   xml.Name    `xml:"episodedetails"`
	Title     string      `xml:"title"`
	Season    int         `xml:"season"`
	Number    int         `xml:"episode"`
	Premiered *dates.Date `xml:"aired"`
	Plot      string      `xml:"plot"`
	Image     string      `xml:"-"`
}

// Filename returns a string suitable to be used as a filename for this episode '<showTitle> S00E00 <title>'.
func (e Episode) Filename(showTitle string) string {
	title := showTitle + " " + e.SeasonAndNumber()
	if e.Title != "" {
		title += " " + e.Title
	}
	return file.Sanitize(title)
}

// SeasonAndNumber returns the formatted string S00E00 with this episode's Season and Number.
func (e Episode) SeasonAndNumber() string {
	season := "S"
	if e.Season <= 9 {
		season += "0"
	}
	season += strconv.Itoa(e.Season)
	number := "E"
	if e.Number <= 9 {
		number += "0"
	}
	number += strconv.Itoa(e.Number)
	return season + number
}

// NFO returns this episode's nfo file xml represention.
func (m Episode) NFO() ([]byte, error) {
	data, err := xml.MarshalIndent(m, "", "\t")
	result := append([]byte(xml.Header), data...)
	return result, err
}
