package libraria

import (
	"encoding/xml"
	"strconv"

	"gitlab.com/bazzz/dates"
	"gitlab.com/bazzz/file"
	"gitlab.com/bazzz/tmdb"
)

func newMovie(input *tmdb.Film) Movie {
	m := Movie{
		Title:      input.Title,
		Premiered:  &input.Premiered,
		Plot:       input.Overview,
		Genres:     input.Genres,
		Poster:     input.Poster,
		Background: input.Backdrop,
	}
	return m
}

// Movie represents a movie.
type Movie struct {
	XMLName    xml.Name    `xml:"movie"`
	Title      string      `xml:"title"`
	Premiered  *dates.Date `xml:"premiered"`
	Plot       string      `xml:"plot"`
	Genres     []string    `xml:"genre"`
	Poster     string      `xml:"-"`
	Background string      `xml:"-"`
}

// Filename returns a string suitable to be used as a filename for this movie '<title> (year)'.
func (m Movie) Filename() string {
	name := m.Title
	if m.Premiered.Year() > 0 {
		name += " (" + strconv.Itoa(m.Premiered.Year()) + ")"
	}
	return file.Sanitize(name)
}

// NFO returns this movie's nfo file xml represention.
func (m Movie) NFO() ([]byte, error) {
	data, err := xml.MarshalIndent(m, "", "\t")
	result := append([]byte(xml.Header), data...)
	return result, err
}
