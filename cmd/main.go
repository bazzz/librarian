package main

import (
	"log"
	"time"

	"github.com/go-co-op/gocron"
	"gitlab.com/bazzz/cfg"
	"gitlab.com/bazzz/libraria"
	priolog "gitlab.com/bazzz/log"
)

func main() {
	priolog.Connect()
	log.SetOutput(priolog.Default())

	config, err := cfg.New()
	if err != nil {
		priolog.Fatal(err)
	}
	settings := libraria.Settings{
		ColdRun:                     config.GetBool("ColdRun"),
		MoviesDestination:           config.Get("MoviesDestination"),
		TVShowsDestination:          config.Get("TVShowsDestination"),
		MusicVideosDestination:      config.Get("MusicVideosDestination"),
		PhotosDestination:           config.Get("PhotosDestination"),
		HomeVideosDestination:       config.Get("HomeVideosDestination"),
		PrefixPhotosDestination:     config.GetBool("PrefixPhotosDestination"),
		PrefixHomeVideosDestination: config.GetBool("PrefixHomeVideosDestination"),
	}
	l, err := libraria.New(config.Get("InputDir"), config.Get("TVDB"), config.Get("IMVDB"), settings)
	if err != nil {
		priolog.Fatal(err)
		return
	}

	l.Process()

	runAt := config.Get("RunAt")
	scheduler := gocron.NewScheduler(time.Local)
	if _, err := scheduler.Every(1).Day().At(runAt).Do(l.Process); err != nil {
		priolog.Fatal(err)
	}
	scheduler.StartBlocking()
}
