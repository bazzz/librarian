package libraria

type Settings struct {
	ColdRun                     bool // If true, processing does not move files from inputDir to destination but writes an empty copy.
	MoviesDestination           string
	TVShowsDestination          string
	MusicVideosDestination      string
	PhotosDestination           string
	HomeVideosDestination       string
	PrefixPhotosDestination     bool
	PrefixHomeVideosDestination bool
}
