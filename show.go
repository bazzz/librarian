package libraria

import (
	"encoding/xml"

	"gitlab.com/bazzz/dates"
	"gitlab.com/bazzz/file"
	"gitlab.com/bazzz/tmdb"
)

func newShow(s *tmdb.Show) *Show {
	return &Show{
		Title:      s.Title,
		Premiered:  &s.Premiered,
		Genres:     s.Genres,
		Overview:   s.Overview,
		Language:   s.OriginalLanguage,
		Poster:     s.Poster,
		Background: s.Backdrop,
	}
}

// Show represents a TV show.
type Show struct {
	XMLName    xml.Name    `xml:"tvshow"`
	Title      string      `xml:"title"`
	Premiered  *dates.Date `xml:"premiered"`
	Genres     []string    `xml:"genre"`
	Overview   string      `xml:"plot"`
	Language   string      `xml:"language"`
	Poster     string      `xml:"-"`
	Background string      `xml:"-"`
}

// Filename returns a string suitable to be used as a filename for this show.
func (e Show) Filename() string {
	return file.Sanitize(e.Title)
}

// NFO returns this show's nfo file xml represention.
func (s Show) NFO() ([]byte, error) {
	data, err := xml.MarshalIndent(s, "", "\t")
	result := []byte(xml.Header)
	result = append(result, data...)
	return result, err
}
