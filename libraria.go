package libraria

import (
	"errors"
	"fmt"
	"log"
	"mime"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	"gitlab.com/bazzz/file"
	"gitlab.com/bazzz/imvdb"
	"gitlab.com/bazzz/slices"
	"gitlab.com/bazzz/tmdb"
)

//const datetimeformat = "060102150405"

var ignoreExtensions = []string{".txt", ".exe", ".nfo", ".part"}
var subtitleExtensions = []string{".srt", ".sub", ".idx"}

// New is the constructor for Libraria.
func New(inputDir string, tmdbKey string, imvdbKey string, settings Settings) (*Libraria, error) {
	homeDir, err := os.UserHomeDir()
	if err != nil {
		return nil, err
	}
	libraria := Libraria{
		inputDir:                    inputDir,
		coldRun:                     settings.ColdRun,
		moviesDestination:           value(settings.MoviesDestination, filepath.Join(homeDir, "Videos", "Movies")),
		tvShowsDestination:          value(settings.TVShowsDestination, filepath.Join(homeDir, "Videos", "TVShows")),
		musicVideosDestination:      value(settings.MusicVideosDestination, filepath.Join(homeDir, "Music", "Videos")),
		photosDestination:           value(settings.PhotosDestination, filepath.Join(homeDir, "Pictures", "Camera")),
		homeVideosDestination:       value(settings.HomeVideosDestination, filepath.Join(homeDir, "Pictures", "Camera")),
		prefixPhotosDestination:     settings.PrefixPhotosDestination,
		prefixHomeVideosDestination: settings.PrefixHomeVideosDestination,
	}
	if tmdbKey != "" {
		tmdbClient, err := tmdb.New(tmdbKey)
		if err != nil {
			return nil, err
		}
		libraria.tmdb = newTMDB(tmdbClient)
	}
	if imvdbKey != "" {
		libraria.imvdb = imvdb.New(imvdbKey)
	}
	return &libraria, nil
}

// Type Libraria is the client for the libraria functions.
type Libraria struct {
	inputDir                    string
	tmdb                        *TMDB
	imvdb                       *imvdb.Client
	coldRun                     bool
	moviesDestination           string
	tvShowsDestination          string
	musicVideosDestination      string
	photosDestination           string
	homeVideosDestination       string
	prefixPhotosDestination     bool
	prefixHomeVideosDestination bool
}

// Process walks inputDir and all sub directories and calls ProcessFile on all found files that are not subtitles. Any warnings or errors are written to the standerd log.
func (l *Libraria) Process() {
	err := filepath.WalkDir(l.inputDir, func(path string, item os.DirEntry, err error) error {
		if item.IsDir() {
			return nil
		}
		ext := filepath.Ext(item.Name())
		if slices.ContainsString(ignoreExtensions, ext) || slices.ContainsString(subtitleExtensions, ext) {
			return nil
		}
		if err := l.ProcessFile(path, item); err != nil {
			log.Println(err)
		} else {
			log.Println("Processed:", item.Name())
		}
		return nil
	})
	if err != nil {
		log.Println("ERROR", err)
	}
}

// ProcessFile handles the file at path.
func (l *Libraria) ProcessFile(path string, item os.DirEntry) (err error) {
	filetype, pos, err := l.getFileType(item)
	if err != nil {
		return fmt.Errorf("could not get filetype: %w", err)
	}
	switch filetype {
	case "movie":
		err = l.handleMovie(path, item, pos)
	case "tvshow":
		err = l.handleTVShow(path, item, pos)
	case "musicvideo":
		err = l.handleMusicVideo(path, item, pos)
	case "photo":
		err = errors.New("not implemented") // l.handlePhoto(path, item)
	case "homevideo":
		err = errors.New("not implemented") // l.handleHomeVideo(path, item)
	}
	if err != nil {
		return fmt.Errorf("WARNING could not handle %v file %v: %w", filetype, path, err)
	}
	return nil
}

// getFileType returns the file type of the item and optionally a position collection of location of the pattern in item.Name.
func (l *Libraria) getFileType(item os.DirEntry) (string, []int, error) {
	name := item.Name()
	ext := filepath.Ext(name)
	mimeType := mime.TypeByExtension(ext)
	if !strings.Contains(mimeType, "/") {
		return "", []int{}, fmt.Errorf("could not read mimetype from %v found: %v", name, mimeType)
	}
	contentType := strings.Split(mimeType, "/")[0]
	switch contentType {
	case "image":
		return "photo", []int{}, nil
	case "video":
		if pos := episodeRegex.FindStringIndex(name); pos != nil {
			return "tvshow", pos, nil
		}
		if pos := yearRegex.FindStringIndex(name); pos != nil {
			// Year found, test if it is not a fulldate as films are never marked with a fulldate to indicate their release year.
			test := fulldateRegex.FindStringIndex(name)
			if test == nil || test[0] != pos[0] {
				return "movie", pos, nil
			}
		}
		// Music Video detection is a little tricky because there does not seem to be a very good pattern.
		if pos := musicvideoRegex.FindStringIndex(name); pos != nil {
			return "musicvideo", pos, nil
		}
		// If the name did not match anything above, we consider it a home video. Perhaps in the future we need to see if the exif of the file has a camera model mentioned or something.
		return "homevideo", []int{}, nil
	}
	return "", []int{}, nil
}

func (l *Libraria) handleMovie(path string, item os.DirEntry, pos []int) error {
	name := cleanText(item.Name())
	year, err := strconv.Atoi(name[pos[0]:pos[1]])
	if err != nil {
		return err
	}
	title := strings.Trim(name[:pos[0]], " ")
	language, _ := detectLanguage(name)
	result, err := l.tmdb.SearchFilm(title, year, language)
	if err != nil {
		return err
	}
	if result == nil {
		return fmt.Errorf("tmdb returned no meta data")
	}
	movie := newMovie(result)
	targetDir := filepath.Join(l.moviesDestination, movie.Filename())
	if err := os.MkdirAll(targetDir, os.ModePerm); err != nil {
		return err
	}
	data, err := movie.NFO()
	if err != nil {
		return err
	}
	if err := os.WriteFile(filepath.Join(targetDir, movie.Filename()+".nfo"), data, os.ModePerm); err != nil {
		return err
	}
	if err := download(movie.Poster, filepath.Join(targetDir, "poster.jpg")); err != nil {
		return err
	}
	if err := download(movie.Background, filepath.Join(targetDir, "fanart.jpg")); err != nil {
		return err
	}
	if err := l.handleSubtitles(path, targetDir, movie.Filename(), false); err != nil {
		return err
	}
	destination := filepath.Join(targetDir, movie.Filename()+filepath.Ext(path))
	if err := l.move(path, destination); err != nil {
		return err
	}
	return nil
}

func (l *Libraria) handleTVShow(path string, item os.DirEntry, pos []int) error {
	name := cleanText(item.Name())
	title := name[:pos[0]]
	title = fulldateRegex.ReplaceAllString(title, "")
	title = strings.Trim(title, " -\t\n")

	year := 0
	if loc := yearRegex.FindStringIndex(title); loc != nil && loc[0] > strings.LastIndex(title, " ") { // see if the showname has a year at the end.
		y, err := strconv.Atoi(title[loc[0]:loc[1]])
		if err == nil {
			year = y
			title = title[:loc[0]]
			title = strings.ReplaceAll(title, "(", "")
			title = strings.ReplaceAll(title, ")", "")
			title = strings.Trim(title, " \t")
		}
	}

	language, _ := detectLanguage(name)
	var showResult *tmdb.Show
	s, err := l.tmdb.SearchShow(title, language)
	if err != nil {
		return err
	}
	if s != nil && isSemanticEqual(title, s.Title) && (year == 0 || year == s.Premiered.Year()) {
		showResult = s
	} else {
		testDir := filepath.Join(l.tvShowsDestination, file.Sanitize(title))
		if file.Exists(testDir) {
			testNFO := filepath.Join(testDir, "tvshow.nfo")
			if file.Exists(testNFO) {
				testPoster := filepath.Join(testDir, "poster.jpg")
				if file.Exists(testPoster) {
					showResult = &tmdb.Show{Title: title}
				}
			}
		}
	}
	if showResult == nil {
		return fmt.Errorf("tmdb returned no meta data, and show is not known at destination")
	}
	show := newShow(showResult)
	targetDir := filepath.Join(l.tvShowsDestination, show.Filename())
	if err := os.MkdirAll(targetDir, os.ModePerm); err != nil {
		return err
	}
	if nfoPath := filepath.Join(targetDir, "tvshow.nfo"); !file.Exists(nfoPath) {
		data, err := show.NFO()
		if err != nil {
			return err
		}
		if err := os.WriteFile(nfoPath, data, os.ModePerm); err != nil {
			return err
		}
	}
	if err := download(show.Poster, filepath.Join(targetDir, "poster.jpg")); err != nil {
		return err
	}
	if err := download(show.Background, filepath.Join(targetDir, "fanart.jpg")); err != nil {
		return err
	}

	episodeIndication := strings.Trim(name[pos[0]:pos[1]], " ")
	episodeIndication = strings.ToLower(episodeIndication)
	seasonNumber := ""
	episodeNumber := ""
	if episodeIndication[0] == 's' && len(episodeIndication) == 3 {
		seasonNumber = episodeIndication[1:3]
		episodeNumber = "0"
	} else if episodeIndication[0] == 's' {
		seasonNumber = episodeIndication[1:3]
		episodeNumber = episodeIndication[4:]
	} else if strings.Contains(episodeIndication, "x") {
		pos := strings.Index(episodeIndication, "x")
		seasonNumber = episodeIndication[0:pos]
		episodeNumber = episodeIndication[pos+1:]
	}
	seasonNr, err := strconv.Atoi(seasonNumber)
	if err != nil {
		return err
	}
	episodeNr, err := strconv.Atoi(episodeNumber)
	if err != nil {
		return err
	}
	episodeResult := &tmdb.Episode{Season: seasonNr, Number: episodeNr}
	if showResult.ID > 0 {
		if e, err := l.tmdb.GetEpisode(showResult.ID, seasonNr, episodeNr, language); err == nil {
			episodeResult = e
		}
	}
	if episodeResult.Title == "" {
		episodeResult.Title = getMaybeTitle(name[pos[1]:])
	}
	if episodeResult.Premiered.Year() == 0 {
		episodeResult.Premiered = getMaybePremiered(name)
	}
	episode := newEpisode(episodeResult)
	episodeFilename := episode.Filename(show.Title)
	data, err := episode.NFO()
	if err != nil {
		return err
	}
	if err := os.WriteFile(filepath.Join(targetDir, episodeFilename+".nfo"), data, os.ModePerm); err != nil {
		return err
	}
	if err := download(episode.Image, filepath.Join(targetDir, episodeFilename+"-thumb.jpg")); err != nil {
		return err
	}
	if err := l.handleSubtitles(path, targetDir, episodeFilename, false); err != nil {
		return err
	}
	destination := filepath.Join(targetDir, episodeFilename+filepath.Ext(path))
	if err := l.move(path, destination); err != nil {
		return err
	}
	return nil
}

func (l *Libraria) handleMusicVideo(path string, item os.DirEntry, pos []int) error {
	name := file.NameWithoutExt(item.Name())
	artist := cleanText(strings.Trim(name[:pos[0]], " "))
	title := cleanText(strings.Trim(name[pos[1]:], " "))
	result := &imvdb.MusicVideo{Title: title, Artists: []imvdb.Artist{{Name: artist}}}
	s, _ := l.imvdb.Find(title, artist)
	if s != nil {
		result = s
	} else {
		return fmt.Errorf("imvdb found no meta data")
	}
	musicvideo := newMusicVideo(result)
	targetDir := filepath.Join(l.musicVideosDestination, musicvideo.Filename())
	if err := os.MkdirAll(targetDir, os.ModePerm); err != nil {
		return err
	}
	data, err := musicvideo.NFO()
	if err != nil {
		return err
	}
	if err := os.WriteFile(filepath.Join(targetDir, musicvideo.Filename()+".nfo"), data, os.ModePerm); err != nil {
		return err
	}
	if err := download(musicvideo.Image, filepath.Join(targetDir, musicvideo.Filename()+"-thumb.jpg")); err != nil {
		return err
	}
	destination := filepath.Join(targetDir, musicvideo.Filename()+filepath.Ext(path))
	if err := l.move(path, destination); err != nil {
		return err
	}
	return nil
}

/*
func (l *Libraria) handlePhoto(path string, item os.DirEntry) error {
	file, err := os.Open(path)
	if err != nil {
		return err
	}
	info, err := exif.Decode(file)
	if err != nil {
		return err
	}
	datetime, err := info.DateTime()
	if err != nil {
		return err
	}
	file.Close()
	filename := datetime.Format(datetimeformat)
	targetDir := l.photosDestination
	if l.prefixPhotosDestination {
		targetDir = filepath.Join(targetDir, datetime.Format("2006-01"))
	}
	if err := os.MkdirAll(targetDir, os.ModePerm); err != nil {
		return err
	}
	destination := filepath.Join(targetDir, filename+filepath.Ext(path))
	if err := l.move(path, destination); err != nil {
		return err
	}
	return nil
}

func (l *Libraria) handleHomeVideo(path string, item os.DirEntry) error {
	info, err := os.Stat(path)
	if err != nil {
		return err
	}
	datetime := info.ModTime()
	filename := datetime.Format(datetimeformat)
	targetDir := l.homeVideosDestination
	if l.prefixHomeVideosDestination {
		targetDir = filepath.Join(targetDir, datetime.Format("2006-01"))
	}
	if err := os.MkdirAll(targetDir, os.ModePerm); err != nil {
		return err
	}
	destination := filepath.Join(targetDir, filename+filepath.Ext(path))
	if err := l.move(path, destination); err != nil {
		return err
	}
	return nil
}
*/

func (l *Libraria) handleSubtitles(videoFilePath string, targetPath string, title string, filenameMatch bool) error {
	subFiles := make([]string, 0)
	fromPath := filepath.Dir(videoFilePath)
	err := filepath.Walk(fromPath, func(fullpath string, fileInfo os.FileInfo, _ error) error {
		fileName := strings.ToLower(fileInfo.Name())
		if fileInfo.IsDir() && fullpath != fromPath && !(fileName == "subs" || fileName == "subtitles") {
			return filepath.SkipDir
		}
		ext := filepath.Ext(fullpath)
		if fileInfo == nil || fileInfo.IsDir() || !slices.ContainsString(subtitleExtensions, ext) {
			return nil
		}
		if filenameMatch && !strings.EqualFold(file.NameWithoutExt(videoFilePath), file.NameWithoutExt(fileInfo.Name())) {
			return nil
		}
		if fileInfo.Size() < 10240 {
			return nil
		}
		subFiles = append(subFiles, fullpath)
		return nil
	})
	if err != nil {
		return err
	}

	for _, subFile := range subFiles {
		fileName := strings.ToLower(file.NameWithoutExt(subFile))
		ext := filepath.Ext(subFile)
		language := ""
		if ext == ".srt" {
			fileName = strings.Replace(fileName, ".", "", -1)
			underscorePos := strings.Index(fileName, "_")
			if underscorePos > -1 {
				fileName = fileName[underscorePos+1:]
			}
			if _, languageName := detectLanguage(fileName); languageName != "" {
				language = "." + languageName
			}
		}
		newname := title + language + ext
		newPath := ""
		i := 0
		for {
			if i > 0 {
				tag := ""
				if strings.HasPrefix(language, ".") {
					tag = language
				} else {
					tag = "."
				}
				tag += strconv.Itoa(i)
				newname = title + tag + ext
			}
			newPath = filepath.Join(targetPath, newname)
			if !file.Exists(newPath) {
				break
			}
			i++
		}
		if newPath == "" {
			continue
		}
		if err := l.move(subFile, newPath); err != nil {
			return fmt.Errorf("could not store subtitles: %w", err)
		}
	}
	return nil
}

func (l *Libraria) move(path string, destination string) error {
	if l.coldRun {
		return os.WriteFile(destination, []byte{}, os.ModePerm)
	}
	return file.Move(path, destination)
}
