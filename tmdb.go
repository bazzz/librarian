package libraria

import (
	"strconv"
	"time"

	"github.com/patrickmn/go-cache"
	"gitlab.com/bazzz/tmdb"
)

const cacheTime = 48 * time.Hour  // Store items for 48 hours.
const cacheClean = 12 * time.Hour // Check every 12 hours whether items should be cleaned.

// TMDB is a wrapper around the tmdb package that adds caching.
type TMDB struct {
	client *tmdb.Client
	cache  *cache.Cache
}

func newTMDB(client *tmdb.Client) *TMDB {
	tmdb := TMDB{
		client: client,
		cache:  cache.New(cacheTime, cacheClean),
	}
	return &tmdb
}

func (t *TMDB) SearchFilm(title string, year int, language string) (*tmdb.Film, error) {
	key := title + strconv.Itoa(year) + language
	if item, ok := t.cache.Get(key); ok {
		return item.(*tmdb.Film), nil
	}
	result, err := t.client.SearchFilm(title, year, language)
	if err != nil {
		return nil, err
	}
	t.cache.SetDefault(key, result)
	return result, nil
}

func (t *TMDB) SearchShow(title string, language string) (*tmdb.Show, error) {
	key := title + language
	if item, ok := t.cache.Get(key); ok {
		return item.(*tmdb.Show), nil
	}
	result, err := t.client.SearchShow(title, language)
	if err != nil {
		return nil, err
	}
	t.cache.SetDefault(key, result)
	return result, nil
}

func (t *TMDB) GetEpisode(showID int, seasonNr int, episodeNr int, language string) (*tmdb.Episode, error) {
	key := strconv.Itoa(showID) + strconv.Itoa(seasonNr) + strconv.Itoa(episodeNr) + language
	if item, ok := t.cache.Get(key); ok {
		return item.(*tmdb.Episode), nil
	}
	result, err := t.client.GetEpisode(showID, seasonNr, episodeNr, language)
	if err != nil {
		return nil, err
	}
	t.cache.SetDefault(key, result)
	return result, nil
}
