module gitlab.com/bazzz/libraria

go 1.20

require (
	github.com/go-co-op/gocron v1.37.0
	github.com/patrickmn/go-cache v2.1.0+incompatible
	gitlab.com/bazzz/cfg v0.0.0-20221031102215-280e27138222
	gitlab.com/bazzz/dates v0.0.0-20220222123917-6929d3f6e4fa
	gitlab.com/bazzz/downloader v0.0.0-20230224094159-28ef69060d42
	gitlab.com/bazzz/file v0.0.0-20220303080129-7b06d0a2508e
	gitlab.com/bazzz/imvdb v0.0.0-20220227085913-a9a5f99dca5d
	gitlab.com/bazzz/log v0.0.0-20220303073850-7bf406fe6b81
	gitlab.com/bazzz/slices v0.0.0-20220301084027-f4cd8972d91e
	gitlab.com/bazzz/tmdb v0.0.0-20240202095145-709195ae6b95
)

require (
	github.com/andybalholm/brotli v1.1.0 // indirect
	github.com/ferhatelmas/levenshtein v0.0.0-20160518143259-a12aecc52d76 // indirect
	github.com/google/uuid v1.6.0 // indirect
	github.com/robfig/cron/v3 v3.0.1 // indirect
	go.uber.org/atomic v1.11.0 // indirect
)
